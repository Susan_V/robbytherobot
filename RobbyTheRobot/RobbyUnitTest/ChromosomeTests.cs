﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobbyGeneticAlgo;

namespace RobbyUnitTest
{
    [TestClass]
    public class ChromosomeTests
    {
        //Seed used for random: 1
        [TestMethod]
        public void TestReproduce()
        {
            Chromosome c = CreateChromosome();
            Chromosome c2 = CreateChromosome2();

            Console.WriteLine(c);
            Console.WriteLine(c2);

            Chromosome[] children = c.Reproduce(c2, Chromosome.SimpleCrossover, 0.20);

            Console.WriteLine(children[0]);
            Console.WriteLine(children[1]);

            Assert.AreEqual(Allele.North, children[0][1], "Expected Allele was North but returned value was different");
            Assert.AreEqual(Allele.South, children[1][1], "Expected Allele was South but returned value was different");
        }

        [TestMethod]
        public void TestEvalFitness()
        {
            Chromosome c = CreateChromosome();
            c.EvalFitness(CreateFitness);

            Assert.AreEqual(2.0, c.Fitness, "Expected fitness was 2.0 but returned value was different");
        }

        [TestMethod]
        public void TestSimpleCrossover()
        {
            Chromosome c = CreateChromosome();
            Chromosome c2 = CreateChromosome2();
            Console.WriteLine(c);
            Console.WriteLine(c2);

            Chromosome[] children = Chromosome.SimpleCrossover(c, c2);
            Console.WriteLine(children[0]);
            Console.WriteLine(children[1]);

            Assert.AreEqual(Allele.North, children[0][1], "Expected Allele was North but returned value was different");
            Assert.AreEqual(Allele.South, children[1][1], "Expected Allele was South but returned value was different");
        }

        [TestMethod]
        public void TestDoubleCrossover()
        {
            Chromosome c = CreateChromosome();
            Chromosome c2 = CreateChromosome2();
            Console.WriteLine(c);
            Console.WriteLine(c2);

            Chromosome[] children = Chromosome.DoubleCrossover(c, c2);
            Console.WriteLine(children[0]);
            Console.WriteLine(children[1]);

            //Check first element of middle
            Assert.AreEqual(Allele.North, children[0][1], "Expected Allele was North but returned value was different");
            Assert.AreEqual(Allele.South, children[1][1], "Expected Allele was South but returned value was different");

            //Check the element after the middle array
            Assert.AreEqual(Allele.Nothing, children[0][4], "Expected Allele was Nothing but returned value was different");
            Assert.AreEqual(Allele.Random, children[1][4], "Expected Allele was Random but returned value was different");
        }

        [TestMethod]
        public void TestCompareTo()
        {
            Chromosome c = CreateChromosome();
            c.EvalFitness(CreateFitness);
            Chromosome c2 = CreateChromosome2();
            c2.EvalFitness(CreateFitness);

            Assert.AreEqual(0, c.CompareTo(c2), "Expected value was 0 (equal values) but returned value was different (either bigger than or smaller than)");
        
        }

        [TestMethod]
        public void TestSort()
        {
            Chromosome c = CreateChromosome();
            c.EvalFitness(CreateFitness2);
            Chromosome c2 = CreateChromosome2();
            c2.EvalFitness(CreateFitness);
            Chromosome[] chromosomes = new Chromosome[2] { c, c2 };

            Array.Sort(chromosomes);

            Assert.AreEqual(2.0, chromosomes[0].Fitness, "Expected first chromosome to be c2 but c1 was the first one (c1 has fitness 3.0 and c2 has fitness 2.0");
        }

        //Create a Chromosome for any test method that needs it
        private Chromosome CreateChromosome()
        {
            Allele[] a = new Allele[] { Allele.North, Allele.South, Allele.East, Allele.West, Allele.Nothing, Allele.PickUp, Allele.Random };
            return new Chromosome(a);
        }

        private Chromosome CreateChromosome2()
        {
            Allele[] a = new Allele[] { Allele.East, Allele.North, Allele.Nothing, Allele.PickUp, Allele.Random, Allele.South, Allele.West };
            return new Chromosome(a);
        }

        //Set the fitness to a simple double value.
        //Needed for TestEvalFitness, TestSort and TestCompareTo
        private double CreateFitness(Chromosome c)
        {
            return 2.0;
        }

        private double CreateFitness2(Chromosome c)
        {
            return 3.0;
        }
    }
}
