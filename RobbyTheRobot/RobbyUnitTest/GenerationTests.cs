﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobbyGeneticAlgo;

namespace RobbyGeneticAlgoUnitTests
{
    [TestClass]
    public class GenerationTests
    {
        //public Generation(int populationSize, int numGenes)
        //theres a try/catch so not necessary
        /*[TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGenerationPopGenes()
        {
            Generation failedGen = new Generation(-1,0);
        }*/


        //public Generation(Chromosome[] members)
        [TestMethod]
        public void TestGenerationArray()
        {
            Allele[] all1 = { Allele.PickUp, Allele.North, Allele.East, Allele.West, Allele.Nothing, Allele.Random };
            Chromosome chrom1 = new Chromosome(all1);
            Allele[] all2 = { Allele.North, Allele.North, Allele.North, Allele.North, Allele.North, Allele.North };
            Chromosome chrom2 = new Chromosome(all2);
            Chromosome[] chromArray = { chrom1, chrom2 };
            Generation gen = new Generation(chromArray);
            Assert.AreEqual(chrom1, gen[0], "Expected chromosome was chrom1 but returned value was different");
            Assert.AreEqual(chrom2, gen[1], "Expected chromosome was chrom2 but returned value was different");
        }


        [TestMethod]
        public void TestEvalFitness()
        {
            //cant really test because the actual value would depend on the board
            Allele[] all1 = { Allele.PickUp, Allele.North, Allele.East, Allele.West, Allele.Nothing, Allele.Random };
            Chromosome chrom1 = new Chromosome(all1);
            Allele[] all2 = { Allele.North, Allele.North, Allele.North, Allele.North, Allele.North, Allele.North };
            Chromosome chrom2 = new Chromosome(all2);
            Chromosome[] chromArray = { chrom1, chrom2};
            Generation gen = new Generation(chromArray);
            gen.EvalFitness(tempFitness1);
            Assert.AreEqual(chrom2, gen[0], "Expected chromosome was chrom2 but returned value was different");
            Assert.AreEqual(chrom1, gen[1], "Expected chromosome was chrom1 but returned value was different");

            gen.EvalFitness(tempFitness2);
            Assert.AreEqual(chrom1, gen[0], "Expected chromosome was chrom1 but returned value was different");
            Assert.AreEqual(chrom2, gen[1], "Expected chromosome was chrom2 but returned value was different");
        }


        [TestMethod]
        public void TestSelectParents()
        {
            Allele[] all1 = { Allele.PickUp, Allele.North, Allele.East, Allele.West, Allele.Nothing, Allele.Random };
            Chromosome chrom1 = new Chromosome(all1);
            Chromosome[] chromArray1 = { chrom1};
            Generation gen1 = new Generation(chromArray1);
            Assert.AreEqual(chrom1, gen1.SelectParent(), "Expected chromosome was chrom1 but returned value was different");

            //statistically both chromosomes would be selected at some point
            Allele[] all2 = { Allele.North, Allele.North, Allele.North, Allele.North, Allele.North, Allele.North };
            Chromosome chrom2 = new Chromosome(all2);
            Chromosome[] chromArray2 = { chrom1, chrom2 };
            Generation gen2 = new Generation(chromArray2);
            gen2.EvalFitness(tempFitness2);
            Assert.AreEqual(chrom2, gen2.SelectParent(), "Expected chromosome was chrom1 but returned value was different");

            gen2.EvalFitness(tempFitness1);
            Assert.AreEqual(chrom1, gen2.SelectParent(), "Expected chromosome was chrom2 but returned value was different");

        }



        //helper methods
        private double tempFitness1(Chromosome c) {
            if (c[0].Equals("PickUp"))
            {
                return 1;
            }
            return 3.3;
        }
        private double tempFitness2(Chromosome c)
        {
            if (c[0].Equals("North"))
            {
                return 1;
            }
            return 3.3;
        }
    }
}
