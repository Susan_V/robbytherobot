﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RobbyGeneticAlgo;

namespace GeneticAlgo
{
    public delegate int AlleleMoveandFitness(Chromosome c, Contents[,] grid, ref int x, ref int y);
    //Added enum
    public enum Contents
    {
        Empty,
        Can,
        Wall
    }

    //Added struct
    public struct DirectionContents
    {
        public Contents N { get; set; }
        public Contents S { get; set; }
        public Contents E { get; set; }
        public Contents W { get; set; }
        public Contents Current { get; set; }
    }
    /// <summary>
    /// This class contains some static helper methods
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Use this field to get any random number. Give it a seed for unit testing.
        /// </summary>
        public static readonly Random rand = new Random();

        /// <summary>
        /// Staring point of the Console application
        /// </summary>
        public static void Main()
        {
            RobbyRobotProblem robby = new RobbyRobotProblem(1000, 200, Helpers.ScoreForAllele);
            robby.GenerationReplaced += Display;
            robby.GenerationReplaced += Print;
            robby.Start();
            Console.ReadKey();
        }

        /// <summary>
        /// TODO Add a Display method
        /// </summary>
        public static void Display(int num, Generation gen)
        {
            Console.WriteLine(num + " " + gen[0].Fitness);        
        }

        public static void Print(int num, Generation gen)
        {
            string textToSend = "";
            switch (num)
            {
                case 1:
                    writeFile(num, gen, ref textToSend);
                    break;
                case 20:
                    writeFile(num, gen, ref textToSend);
                    break;
                case 100:
                    writeFile(num, gen, ref textToSend);
                    break;
                case 200:
                    writeFile(num, gen, ref textToSend);
                    break;
                case 500:
                    writeFile(num, gen, ref textToSend);
                    break;
                case 1000:
                    writeFile(num, gen, ref textToSend);
                    break;
            }
            if (!textToSend.Equals(""))
            {
                File.WriteAllText("Generation" + num + "Contents.txt", textToSend);
            }
        }

        private static void writeFile(int num, Generation gen, ref string textToSend)
        {
            //Generation number
            textToSend += num + "\n";
            //The Alleles
            textToSend += gen[0].ToString() + "\n";
            //Number of actions, varies for each generation
            switch (num)
            {
                case 1:
                    textToSend += "20";
                    break;
                case 20:
                    textToSend += "30";
                    break;
                case 100:
                    textToSend += "50";
                    break;
                case 200:
                    textToSend += "100";
                    break;
                case 500:
                    textToSend += "200";
                    break;
                case 1000:
                    textToSend += "200";
                    break;
            }
        }

        /// <summary>
        /// Applies the fitness function to move Robby through the given testgrid for numActions moves based on the
        /// Chromosome. The fitness function returns the fitness score after each move.
        /// </summary>
        /// <param name="testgrid"> Testgrid for Robby</param>
        /// <param name="c">Chromosome being tested</param>
        /// <param name="numActions">Number of moves that Robby is allowed</param>
        /// <param name="f">Fitness fuction that makes 1 move</param>
        /// <returns></returns>
        public static int RunRobbyInGrid(Contents[,] testgrid, Chromosome c, int numActions, AlleleMoveandFitness f)
        {
            //starting point
            int x = Helpers.rand.Next(0, testgrid.GetLength(0));
            int y = Helpers.rand.Next(0, testgrid.GetLength(1));

            //make a copy of the grid
            Contents[,] grid = (Contents[,])testgrid.Clone();

            //make moves, count score
            int score = 0;
            for (int i = 0; i < numActions; i++)
            {
                //get score
                score += f(c, grid, ref x, ref y);
            }
            return score;
        }

        /// <summary>
        /// Used to fill up a DirectionsContent struct based on Robby's position in the 
        /// grid and what is immediately adjacent to him.
        /// </summary>
        /// <param name="x">Robby's x coordinates</param>
        /// <param name="y">Robby's y coordinates</param>
        /// <param name="grid">The test grid where Robby is</param>
        /// <returns>What Robby sees in all directions plus current</returns>
        public static DirectionContents LookAround(int x, int y, Contents[,] grid)
        {
            //what do you see?
            DirectionContents dir = new DirectionContents();
            //where are the walls?
            if (y == 0)
                dir.N = Contents.Wall; //wall
            else
                dir.N = grid[x, y - 1];

            if (y == grid.GetLength(1) - 1)
                dir.S = Contents.Wall;
            else
                dir.S = grid[x, y + 1];

            if (x == grid.GetLength(0) - 1)
                dir.E = Contents.Wall;
            else
                dir.E = grid[x + 1, y];

            if (x == 0)
                dir.W = Contents.Wall;
            else
                dir.W = grid[x - 1, y];

            dir.Current = grid[x, y];

            return dir;
        }
        /// <summary>
        /// Translates Robby's DirectionContents into the appropriate gene index
        /// </summary>
        /// <param name="dir">The struct returned by LookAround</param>
        /// <returns>The index of the Chromosome</returns>
        public static int FindGeneIndex(DirectionContents dir)
        {
            int gene = 0;
            gene += getIndexForDirection(dir.N, 4);
            gene += getIndexForDirection(dir.S, 3);
            gene += getIndexForDirection(dir.E, 2);
            gene += getIndexForDirection(dir.W, 1);
            gene += getIndexForDirection(dir.Current, 0);
            return gene;
        }
        /// <summary>
        /// Used to build up the index of the gene in the Chromosome
        /// </summary>
        /// <param name="content">Content in a given direction</param>
        /// <param name="power">Exponent of 10</param>
        /// <returns>Partial calculation of the gene's index</returns>
        private static int getIndexForDirection(Contents content, int power)
        {
            if (content == Contents.Empty)
                return 0;
            if (content == Contents.Can)
                return (int)(Math.Pow(3, power));
            //Wall
            return (int)(2 * Math.Pow(3, power));
        }
        /// <summary>
        /// Used to generate a single test grid filled with cans in random locations. Half of 
        /// the grid (rounded down) will be filled with cans.
        /// </summary>
        /// <param name="gridSize">Width or height of a square grid</param>
        /// <returns>Rectangular array of Contents filled with 50% Cans, and 50% Empty </returns>
        public static Contents[,] GenerateRandomTestGrid(int gridSize)
        {
            ///TODO
            int numOfCans = (int)(Math.Pow(gridSize, 2) / 2);
            Contents[,] testGrid = new Contents[gridSize, gridSize];

            //Fill with empty and walls
            for(int x = 0; x < testGrid.GetLength(0); x++)
            {
                for(int y = 0; y < testGrid.GetLength(1); y++)
                {
                    if(x == 0 || y == 0)
                    {
                        testGrid[x, y] = Contents.Wall;
                    }
                    else if(x == gridSize - 1 || y == gridSize - 1)
                    {
                        testGrid[x, y] = Contents.Wall;
                    }
                    else
                    {
                        testGrid[x, y] = Contents.Empty;
                    }
                }
            }
            fillCans(ref testGrid, numOfCans);
            return testGrid;
        }

        private static void fillCans(ref Contents[,] testGrid, int numOfCans)
        {
            int currentCan = 1;
            int x = rand.Next(testGrid.GetLength(0));
            int y = rand.Next(testGrid.GetLength(1));
            //Then fill with cans, making sure it reaches the required amount
            while (currentCan <= numOfCans)
            {
                if(testGrid[x, y] == Contents.Empty)
                {
                    testGrid[x, y] = Contents.Can;
                    currentCan++;
                }
                x = rand.Next(testGrid.GetLength(0));
                y = rand.Next(testGrid.GetLength(1));
            }
        }

        /// <summary>
        /// Moves Robby and returns to score for a single move in the grid, given the
        /// Chromosome and his position.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="grid"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static int ScoreForAllele(Chromosome c, Contents[,] grid, ref int x, ref int y)
        {
            DirectionContents dir = Helpers.LookAround(x, y, grid);
            //find the gene
            int gene = Helpers.FindGeneIndex(dir);
            //find the move
            Allele move = c[gene];
            bool done;
            do
            {
                done = true;
                switch (move)
                {
                    case Allele.North://move north
                        if (dir.N == Contents.Wall)
                            return -5;
                        y -= 1;
                        break;
                    case Allele.South://move south
                        if (dir.S == Contents.Wall)
                            return -5;
                        y += 1;
                        break;
                    case Allele.East: //move east
                        if (dir.E == Contents.Wall)
                            return -5;
                        x += 1;
                        break;
                    case Allele.West: //move west
                        if (dir.W == Contents.Wall)
                            return -5;
                        x -= 1;
                        break;
                    case Allele.Nothing: //do nothong
                        break;
                    case Allele.PickUp: //pick up can
                        if (grid[x, y] == Contents.Can) //there is a can
                        {
                            grid[x, y] = Contents.Empty;
                            return +10;
                        }
                        else
                            return -1; //penalty for picking up nothing
                    case Allele.Random: //random move
                        done = false;
                        int num = rand.Next(0, Enum.GetNames(typeof(Allele)).Length);
                        move = (Allele)num;
                        break;
                }
            }
            while (!done);
            return 0;
        }
    }
}
