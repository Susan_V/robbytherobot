﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GeneticAlgo;

namespace RobbyGeneticAlgo
{
    public class Generation
    {
        private Chromosome[] chromosomes;
        public Chromosome this[int index]
        {
            get { return this.chromosomes[index]; }
        }

        public Generation(int populationSize, int numGenes)
        {
            //Set to default value if one of them isn't valid
            try
            {
                if(populationSize <= 0 || numGenes <= 0)
                {
                    throw new ArgumentException("Population and number of genes should be more than 0");
                }
            }
            catch (ArgumentException)
            {
                if (populationSize <= 0) populationSize = 200;
                if (numGenes <= 0) numGenes = 243;
            }
            this.chromosomes = new Chromosome[populationSize];
            for (int x = 0; x < this.chromosomes.Length; x++)
            {
                /* Not sure this is what its supposed to do 
                 * but im guessing this is where 
                 * the numGenes is needed */

                //new chromosome each time
                Chromosome tempChrom = new Chromosome(numGenes);
                this.chromosomes[x] = tempChrom;
            }
        }

        public Generation(Chromosome[] members)
        {
            this.chromosomes = new Chromosome[members.Length];
            for (int x=0; x< members.Length; x++) 
                this.chromosomes[x] = members[x];
        }

        public void EvalFitness(Fitness f)
        {
            foreach (Chromosome item in this.chromosomes) 
                item.EvalFitness(f);
            Array.Sort(this.chromosomes);
            Array.Reverse(this.chromosomes);
        }

        public Chromosome SelectParent()
        {
            int[] indexes = new int[10];
            int x = 0;
            while(x!=indexes.Length)
            {
                int randNum = Helpers.rand.Next(this.chromosomes.Length);
                indexes[x] = randNum;
                x++;
            }
            Array.Sort(indexes);
            return this.chromosomes[indexes[0]];
        }
    }
}
