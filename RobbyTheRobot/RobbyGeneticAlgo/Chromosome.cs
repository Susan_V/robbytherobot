﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GeneticAlgo;

namespace RobbyGeneticAlgo
{
    //Delegates used in the Chromosome class
    public delegate double Fitness(Chromosome c);
    public delegate Chromosome[] Crossover(Chromosome a, Chromosome b);

    //The robot's moves
    public enum Allele
    {
        North,
        South,
        East,
        West,
        Nothing,
        PickUp,
        Random
    }

    public class Chromosome : IComparable<Chromosome>
    {
        private Allele[] genes;
        public Allele this[int index]
        {
            get { return this.genes[index]; }
        }

        public double Fitness { get; private set; }

        /// <summary>
        /// Create an array of random alleles based on the given length
        /// </summary>
        /// <param name="length"></param>
        public Chromosome(int length)
        {
            this.genes = new Allele[length];
            int numOfEnums = Enum.GetNames(typeof(Allele)).Length;
            for(int i = 0; i < this.genes.Length; i++)
            {
                int nextIndex = Helpers.rand.Next(numOfEnums);
                //Gets the enum associated with the index Ex: 0 = North, 1 = South
                this.genes[i] = (Allele)nextIndex;
            }
            this.Fitness = 0;
        }

        /// <summary>
        /// Create an array of alleles by performing a deep copy of the given Allele array
        /// </summary>
        /// <param name="gene"></param>
        public Chromosome(Allele[] gene)
        {
            this.genes = new Allele[gene.Length];
            for(int i = 0; i < gene.Length; i++)
            {
                this.genes[i] = gene[i];
            }
        }

        public Chromosome[] Reproduce(Chromosome spouse, Crossover f, double mutationRate)
        {
            //Set to default value if one of them isn't valid
            try
            {
                if (mutationRate <= 0)
                {
                    throw new ArgumentException("The mutation rate should be more than 0");
                }
            }
            catch (ArgumentException)
            {
                mutationRate = 0.05;
            }

            Chromosome[] children = f(this, spouse);

            int numOfEnums = Enum.GetNames(typeof(Allele)).Length;
            int nextIndex = 0;
            //Mutate the children
            for (int i = 0; i < children[0].genes.Length; i++)
            {
                if (AllowMutation(mutationRate))
                {
                    nextIndex = Helpers.rand.Next(numOfEnums);
                    children[0].genes[i] = (Allele)nextIndex;
                }
            }
            for(int i = 0; i < children[1].genes.Length; i++)
            {
                if (AllowMutation(mutationRate))
                {
                    nextIndex = Helpers.rand.Next(numOfEnums);
                    children[1].genes[i] = (Allele)nextIndex;
                }
            }
            return children;
        }

        /// <summary>
        /// Determine if we allow mutation for one gene
        /// </summary>
        /// <param name="mutationRate">The chance that the mutation will happen</param>
        /// <returns>true or false depending on the percentage</returns>
        private bool AllowMutation(double mutationRate)
        {
            int percentage = (int)mutationRate * 100;
            if(percentage > 100)
            {
                percentage = 100;
            }
            return Helpers.rand.Next(100) <= percentage;
        }

        /// <summary>
        /// Invokes the Fitness delegate which has the RobbyFitness in the RobbyRobotProblem class,
        /// then gives the returned double in the Fitness property
        /// </summary>
        /// <param name="f"></param>
        public void EvalFitness(Fitness f)
        {
            double returnedFitness = f(this);
            this.Fitness = returnedFitness;
        }

        //SimpleCrossover and DoubleCrossover are invoked using the Crossover delegate, which is used in the Reproduce method
        public static Chromosome[] SimpleCrossover(Chromosome a, Chromosome b)
        {
            int genesLength = a.genes.Length;
            int randomIndex = Helpers.rand.Next(genesLength);

            Allele[][] firstSplit = a.SimpleSplit(a.genes, randomIndex);
            Allele[][] secondSplit = b.SimpleSplit(b.genes, randomIndex);

            Allele[] firstChild = a.JoinGenesSimple(firstSplit[0], secondSplit[1]);
            Allele[] secondChild = b.JoinGenesSimple(secondSplit[0], firstSplit[1]);

            Chromosome[] children = new Chromosome[2] { new Chromosome(firstChild), new Chromosome(secondChild) };

            return children;
        }

        /// <summary>
        /// Splits the gene array for a simple crossover, puts the result in a jagged array
        /// </summary>
        /// <param name="toSplit">The array to split</param>
        /// <param name="splitIndex">The index where to split</param>
        /// <returns>The splitted genes inside a jagged array</returns>
        private Allele[][] SimpleSplit(Allele[] toSplit, int splitIndex)
        {
            Allele[][] splittedArrays = new Allele[2][];
            splittedArrays[0] = new Allele[splitIndex];
            Allele[] firstPart = new Allele[splitIndex];
            for (int i = 0; i < splitIndex; i++)
            {
                splittedArrays[0][i] = toSplit[i];
            }

            splittedArrays[1] = new Allele[toSplit.Length - splitIndex];
            int index = 0;
            for (int i = splitIndex; i < toSplit.Length; i++)
            {
                splittedArrays[1][index] = toSplit[i];
                index++;
            }
            return splittedArrays;
        }

        /// <summary>
        /// Joining genes together using simple crossover
        /// </summary>
        /// <param name="first">The first couple of genes</param>
        /// <param name="second">The second couple of genes</param>
        /// <returns>Joined genes in a single Allele array</returns>
        private Allele[] JoinGenesSimple(Allele[] first, Allele[] second)
        {
            Allele[] joinedGenes = new Allele[first.Length + second.Length];
            for(int i = 0; i < first.Length; i++)
            {
                joinedGenes[i] = first[i];
            }
            int index = 0;
            for(int j = first.Length; j < joinedGenes.Length; j++)
            {
                joinedGenes[j] = second[index];
                index++;
            }
            return joinedGenes;
        }

        public static Chromosome[] DoubleCrossover(Chromosome a, Chromosome b)
        {
            int genesLength = a.genes.Length;
            int halfIndex = genesLength/ 2;
            //Prevent the splitted middle array to start at 0 and to end at genes.length
            int firstHalfIndex = Helpers.rand.Next(1, halfIndex);
            int secondHalfIndex = Helpers.rand.Next(halfIndex, genesLength - 1);


            Allele[] firstChild = new Allele[genesLength];
            Allele[] secondChild = new Allele[genesLength];

            //Basic swapping if firstHalfIndex and secondHalfIndex are equal
            if(firstHalfIndex == secondHalfIndex)
            {
                firstChild = a.DoubleJoinSwap(a.genes, b.genes[halfIndex], halfIndex);
                secondChild = b.DoubleJoinSwap(b.genes, a.genes[halfIndex], halfIndex);
            }
            else
            {
                Allele[][] firstSplit = a.DoubleSplit(a.genes, firstHalfIndex, secondHalfIndex);
                Allele[][] secondSplit = b.DoubleSplit(b.genes, firstHalfIndex, secondHalfIndex);
                firstChild = a.JoinGenesDouble(firstSplit[0], secondSplit[1], firstHalfIndex, secondHalfIndex);
                secondChild = b.JoinGenesDouble(secondSplit[0], firstSplit[1], firstHalfIndex, secondHalfIndex);
            }

            Chromosome[] children = new Chromosome[2] { new Chromosome(firstChild), new Chromosome(secondChild) };

            return children;
        }

        /// <summary>
        /// Splitting the genes for double crossover
        /// </summary>
        /// <param name="toSplit">The array to split</param>
        /// <param name="firstIndex">Starting index of the splitted array in the middle</param>
        /// <param name="secondIndex">Ending index of the splitted array in the middle</param>
        /// <returns>The splitted genes inside a jagged array</returns>
        private Allele[][] DoubleSplit(Allele[] toSplit, int firstIndex, int secondIndex)
        {
            int splitSize = (secondIndex - firstIndex) + 1;
            Allele[][] splittedArrays = new Allele[2][];
            splittedArrays[0] = new Allele[toSplit.Length - splitSize];
            splittedArrays[1] = new Allele[splitSize];

            //First array is the elements outside the selected range
            int index = 0;
            for (int i = 0; i < firstIndex; i++)
            {
                splittedArrays[0][index] = toSplit[i];
                index++;
            }
            for(int i = secondIndex + 1; i < toSplit.Length; i++)
            {
                splittedArrays[0][index] = toSplit[i];
                index++;
            }

            //Second array is the selected range
            index = 0;
            for (int i = firstIndex; i < secondIndex + 1; i++)
            {
                splittedArrays[1][index] = toSplit[i];
                index++;
            }
            
            return splittedArrays;
        }

        /// <summary>
        /// Joining genes together using double crossover
        /// </summary>
        /// <param name="first">The first couple of genes (the outside array)</param>
        /// <param name="second">The second couple of genes (the middle)</param>
        /// <param name="firstIndex">Starting index for inserting the second array</param>
        /// <param name="secondIndex">Ending index for inserting the second array</param>
        /// <returns>The joined genes in a single Allele array</returns>
        private Allele[] JoinGenesDouble(Allele[] first, Allele[] second, int firstIndex, int secondIndex)
        {
            Allele[] joinedGenes = new Allele[first.Length + second.Length];
            int index = 0;
            int outsideIndex = 0;
            for(int i = 0; i < joinedGenes.Length; i++)
            {
                if(i >= firstIndex && i <= secondIndex)
                {
                    joinedGenes[i] = second[index];
                    index++;
                }
                else if(i > secondIndex)
                {
                    joinedGenes[i] = first[outsideIndex];
                    outsideIndex++;
                }
                else
                {
                    joinedGenes[i] = first[outsideIndex];
                    outsideIndex++;
                }
            }
            return joinedGenes;
        }

        /// <summary>
        /// Joining genes together with a simple swap
        /// </summary>
        /// <param name="original">The previous Allele array</param>
        /// <param name="singleElement">The gene to be swapped</param>
        /// <param name="halfIndex">The index at the middle of the original array</param>
        /// <returns>A joined Allele array done with a simple swap</returns>
        private Allele[] DoubleJoinSwap(Allele[] original, Allele singleElement, int halfIndex)
        {
            Allele[] newArray = new Allele[original.Length];
            for(int i = 0; i < newArray.Length; i++)
            {
                if(i == halfIndex)
                {
                    newArray[i] = singleElement;
                }
                else
                {
                    newArray[i] = original[i];
                }
            }
            return newArray;
        }

        
        /// <summary>
        /// Overrides the CompareTo method, helps with sorting an array of Chromosomes
        /// </summary>
        /// <param name="other"></param>
        /// <returns>1 if this.Fitness is larger, -1 if smaller, 0 if equal </returns>
        public int CompareTo(Chromosome other)
        {
            if(this.Fitness > other.Fitness)
            {
                return 1;
            }
            else if(this.Fitness < other.Fitness)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Overriding the toString method
        /// </summary>
        /// <returns>this.genes in a string, each element separated with a comma</returns>
        public override string ToString()
        {
            string toPrint = "";
            for(int i = 0; i < this.genes.Length; i++)
            {
               if(i == this.genes.Length - 1)
               {
                    toPrint += this.genes[i].ToString();
               }
               else
               {
                    toPrint += this.genes[i].ToString() + ",";
               }
            }
            return toPrint;
        }
    }
}
