﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GeneticAlgo;

namespace RobbyGeneticAlgo
{
    public delegate void GenerationEventHandler(int num, Generation g);

    public class RobbyRobotProblem
    {
        public GenerationEventHandler GenerationReplaced;

        //We need a Generation field for this class
        private Generation generation;

        private int numGenerations;
        private int popSize;
        private AlleleMoveandFitness f;
        private int numActions;
        private int numTestGrids;
        private int gridSize;
        private int numGenes;
        private double eliteRate;
        private double mutationRate;
        //To know which generation the program is currently at
        private int currentGeneration;
        //Test grids to evaluate moves
        private Contents[][,] testGrids;

        //The method to trigger the event
        protected virtual void OnGenerationReplaced()
        {
            GenerationReplaced?.Invoke(this.currentGeneration, this.generation);
        }

        public RobbyRobotProblem(int numGenerations, int popSize, AlleleMoveandFitness f, int numActions = 200, int numTestGrids = 100,
            int gridSize = 10, int numGenes = 243, double eliteRate = 0.05, double mutationRate = 0.05)
        {
            //Set to default value if one of them isn't valid
            try
            {
                if (numGenerations <= 0 || numActions <= 0 || numTestGrids <= 0 || gridSize <= 0 || eliteRate <= 0)
                {
                    throw new ArgumentException("The number of generations, actions, test grids, the grid size and the elite rate should be more than 0");
                }
            }
            catch(ArgumentException)
            {
                if (numGenerations <= 0) numGenerations = 1000;
                if (numActions <= 0) numActions = 200;
                if (numTestGrids <= 0) numTestGrids = 100;
                if (gridSize <= 0) gridSize = 10;
                if (eliteRate <= 0) eliteRate = 0.05;
            }
            
            this.popSize = popSize;
            this.numGenes = numGenes;
            this.numGenerations = numGenerations;
            this.f = f;
            this.numActions = numActions;
            this.numTestGrids = numTestGrids;
            this.gridSize = gridSize;
            this.eliteRate = eliteRate;
            this.mutationRate = mutationRate;
            this.currentGeneration = 1;

            this.generation = new Generation(this.popSize, this.numGenes);
        }
        
        public void Start()
        {
            while (this.currentGeneration <= this.numGenerations)
            {
                EvalFitness(RobbyFitness);

                //Invoke GenerationReplaced (The delegate at the top. First call a method, like the slide examples wrote)
                OnGenerationReplaced();

                this.currentGeneration++;
                this.GenerateNextGeneration();
            }
        }

        public void EvalFitness(Fitness f)
        {
            //Create test grids
            this.testGrids = new Contents[this.numTestGrids][,];
            for(int i = 0; i < this.numTestGrids; i++)
            {
                this.testGrids[i] = Helpers.GenerateRandomTestGrid(this.gridSize);
            }
            this.generation.EvalFitness(f);
        }

        public void GenerateNextGeneration()
        {
            //New solutions with the same pop size
            Chromosome[] newSolutions = new Chromosome[this.popSize];
            //Get the number of elites. Increase it by one if the number returned isn't even
            int numberOfElites = (int)(this.popSize * this.eliteRate);
            if((numberOfElites % 2) == 1)
            {
                numberOfElites++;
            }

            //Get the elites. Put those elites in the new solutions
            Chromosome[] elites = new Chromosome[numberOfElites];
            for(int i = 0; i < numberOfElites; i++)
            {
                elites[i] = this.generation.SelectParent();
                newSolutions[i] = elites[i];
            }

            //Get the children. Put the new children in the rest of the newSolutions array
            int numberOfChildren = popSize - numberOfElites;
            int randomParent = Helpers.rand.Next(numberOfElites);
            int parentChosen = randomParent;
            for (int i = numberOfElites; i < this.popSize; i++)
            {
                //The parents need to be different
                Chromosome firstParent = elites[randomParent];
                while(randomParent == parentChosen)
                {
                    randomParent = Helpers.rand.Next(numberOfElites);
                }
                Chromosome secondParent = elites[randomParent];
                Crossover chosenCrossover = ChooseCrossover();
                Chromosome[] children = firstParent.Reproduce(secondParent, chosenCrossover, this.mutationRate);
                newSolutions[i] = children[0];
                newSolutions[i + 1] = children[1];

                randomParent = Helpers.rand.Next(numberOfElites);
                parentChosen = randomParent;
                //Storing two children each time so iterate the index one more
                i++;
            }

            Generation newGen = new Generation(newSolutions);
            this.generation = newGen;
        }

        /// <summary>
        /// For each chromosome, in each test grid, calculate its average fitness
        /// </summary>
        /// <param name="c">The chromosome to test</param>
        /// <returns>The chromosome's average fitness</returns>
        public double RobbyFitness(Chromosome c)
        {
            double totalScore = 0;
            for(int i = 0; i < this.testGrids.Length; i++)
            {
                totalScore += Helpers.RunRobbyInGrid(this.testGrids[i], c, this.numActions, this.f);
            }
            return totalScore / this.numTestGrids;
        }

        /// <summary>
        /// Choosing one of the crossovers of Chromosome
        /// </summary>
        /// <returns>The crossover to use</returns>
        private static Crossover ChooseCrossover() {
            if (Helpers.rand.Next(1) == 1) {
                return Chromosome.SimpleCrossover;
            }
            return Chromosome.DoubleCrossover;
        }
    }
}
