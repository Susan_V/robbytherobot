﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RobbyGeneticAlgo;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System.IO;
using GeneticAlgo;
using System.Threading;

namespace Robby
{
    public class SimulationSprite : DrawableGameComponent
    {
        private Generation[] generations;
        private int[] genNums;
        private int[] genActions;
        private Contents[][,] testGrids;
        private Contents[,] currentGrid;
        private int currentGen;
        private int currentScore;
        private int robbyX;
        private int robbyY;
        private int numActions;

        private Game1 game;

        private SpriteBatch spriteBatch;

        private Texture2D imageEmpty;
        private Texture2D imageWall;
        private Texture2D imageCan;
        private Texture2D imageRobby;
        private SpriteFont font;

        private float delay;
        private float timeLeft;

        public SimulationSprite(Game1 game) : base(game)
        {
            this.game = game;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            //Loading the images and font
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.imageEmpty = game.Content.Load<Texture2D>("empty");
            this.imageWall = game.Content.Load<Texture2D>("wall");
            this.imageCan = game.Content.Load<Texture2D>("can");
            this.imageRobby = game.Content.Load<Texture2D>("robby");
            this.font = game.Content.Load<SpriteFont>("font");

            //Get the paths of the generation files
            string[] paths = new string[6];
            paths[0] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation1Contents.txt";
            paths[1] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation20Contents.txt";
            paths[2] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation100Contents.txt";
            paths[3] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation200Contents.txt";
            paths[4] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation500Contents.txt";
            paths[5] = "..\\..\\..\\..\\..\\RobbyGeneticAlgo\\bin\\debug\\Generation1000Contents.txt";

            this.generations = new Generation[paths.Length];
            this.genNums = new int[paths.Length];
            this.genActions = new int[paths.Length];

            getGenerations(paths);
            //Create random test grids for each generation
            this.testGrids = new Contents[paths.Length][,];
            for(int i = 0; i < this.testGrids.Length; i++)
            {
                this.testGrids[i] = Helpers.GenerateRandomTestGrid(10);
            }

            this.currentGen = 0;
            this.currentGrid = this.testGrids[this.currentGen];
            //Robby's initial position, determined at random. Have its position inside the surrounding wall
            this.robbyX = Helpers.rand.Next(1, this.currentGrid.GetLength(0) - 1);
            this.robbyY = Helpers.rand.Next(1, this.currentGrid.GetLength(1) - 1);
            this.numActions = 0;
            this.currentScore = 0;
            this.delay = (float)0.2;
            this.timeLeft = delay;
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            //Prevent any updates if we reach the final generation
            if (this.currentGen >= this.generations.Length)
            {
                return;
            }
            //Slowing down each update of the game
            //Source: https://gamedev.stackexchange.com/questions/81411/how-to-have-an-event-occur-after-x-seconds
            float time = (float)gameTime.ElapsedGameTime.TotalSeconds;
            this.timeLeft -= time;
            if (timeLeft <= 0)
            {
                //Change the score in each action
                if (this.numActions < this.genActions[this.currentGen])
                {
                    //Using ref will help us get the position of Robby
                    this.currentScore += Helpers.ScoreForAllele(this.generations[this.currentGen][0], this.currentGrid, ref this.robbyX, ref this.robbyY);
                    this.numActions++;
                }
                else
                {
                    this.currentGen++;
                    //Exit the method after the final generation was done
                    if (this.currentGen >= this.generations.Length)
                    {
                        this.currentGen--;
                        return;
                    }
                    //Reset the number of actions, the score and change the grid to display
                    this.numActions = 0;
                    this.currentGrid = this.testGrids[this.currentGen];
                    this.currentScore = 0;
                    this.robbyX = Helpers.rand.Next(1, this.currentGrid.GetLength(0) - 1);
                    this.robbyY = Helpers.rand.Next(1, this.currentGrid.GetLength(1) - 1);
                }
                this.timeLeft = this.delay;
            }
            base.Update(gameTime);
        }

        //With each update, draw any changes on the grid, the score and the generation
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            //This nested loop prints the whole grid of the game to the Monogame window
            for(int x = 0; x < this.currentGrid.GetLength(0); x++)
            {
                for(int y = 0; y < this.currentGrid.GetLength(1); y++)
                {
                    if (x == this.robbyX && y == this.robbyY)
                    {
                        spriteBatch.Draw(this.imageRobby, new Rectangle(x * 32, y * 32, 32, 32), Color.White);
                    }
                    else if(this.currentGrid[x,y] == Contents.Can)
                    {
                        spriteBatch.Draw(this.imageCan, new Rectangle(x * 32, y * 32, 32, 32), Color.White);
                    }
                    else if(this.currentGrid[x,y] == Contents.Wall)
                    {
                        spriteBatch.Draw(this.imageWall, new Rectangle(x * 32, y * 32, 32, 32), Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(this.imageEmpty, new Rectangle(x * 32, y * 32, 32, 32), Color.White);
                    }
                }
            }
            //Display the score and the generation number to the Monogame Window
            spriteBatch.DrawString(this.font, "Generation " + this.genNums[this.currentGen].ToString(), new Vector2(350, 10), Color.Black);
            spriteBatch.DrawString(this.font, "Score: " + this.currentScore.ToString(), new Vector2(350, 30), Color.Black);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        /// <summary>
        /// Get the generations from the files in RobbyGeneticAlgo
        /// </summary>
        /// <param name="paths">The locations of each file containing the 1st, 20th, 100th, 200th, 500th and 1000th generations</param>
        private void getGenerations(string[] paths)
        {
            string[] lines;
            string[] alleles;
            //Generation has an indexer for Chromosome, so the single chromosome has to be stored in an array of length 1
            Chromosome[] c = new Chromosome[1];
            
            //First line: generation number
            //Second line: alleles of the best chromosome
            //Third line: Maximum number of actions
            for(int i = 0; i < paths.Length; i++)
            {
                lines = File.ReadAllLines(paths[i]);
                this.genNums[i] = Convert.ToInt32(lines[0]);
                alleles = lines[1].Split(',');
                this.genActions[i] = Convert.ToInt32(lines[2]);
                c[0] = createChromosome(alleles);
                this.generations[i] = new Generation(c);
            }
        }

        /// <summary>
        /// Copy the alleles string array to an Allele array
        /// </summary>
        /// <param name="alleles">The string array to copy into an Allele array</param>
        /// <returns>A new Chromosome with the copied array</returns>
        private Chromosome createChromosome(string[] alleles)
        {
            Allele[] allelesToSend = new Allele[alleles.Length];
            for(int i = 0; i < alleles.Length; i++)
            {
                switch (alleles[i])
                {
                    case "North":
                        allelesToSend[i] = Allele.North;
                        break;
                    case "South":
                        allelesToSend[i] = Allele.South;
                        break;
                    case "East":
                        allelesToSend[i] = Allele.East;
                        break;
                    case "West":
                        allelesToSend[i] = Allele.West;
                        break;
                    case "Nothing":
                        allelesToSend[i] = Allele.Nothing;
                        break;
                    case "PickUp":
                        allelesToSend[i] = Allele.PickUp;
                        break;
                    case "Random":
                        allelesToSend[i] = Allele.Random;
                        break;
                }
            }
            return new Chromosome(allelesToSend);
        }
    }
}
