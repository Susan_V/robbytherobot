# RobbyTheRobot
Academic project done at Dawson College.

* Along with a teammate, programmed a robot that moves around automatically and pick up cans scattered around efficiently, using the Monogame framework
* Programmed the whole Monogame logic of the project
* Worked on the Chromosome class and test cases
* Worked on the RobbyRobotProblem class along with teammate
* Helpers class provided by instructor
